from typing import Optional, List, Union
from urllib.parse import unquote
import xml.etree.ElementTree as ET
from datetime import datetime
from object.ca import Ca
from object.key import Key
from object.cert import Cert
from object.crl import Crl
from url.fileloader import Fileloader


class EtrustLoaderXml:
    '''
    Class for loading an xml file
    '''
    URL = "https://e-trust.gosuslugi.ru/app/scc/portal/api/v1/portal/ca/getxml"

    def __init__(self, xml_file: str, xml_dir: str) -> None:
        self.loader = Fileloader
        self.xml_file = xml_file
        self.xml_dir = xml_dir

    def get_file(self) -> bool:
        """
        #### Retrieves path from XML file
        """
        path_xml = self._load_xml()
        if path_xml:
            return True

        return False

    def _load_xml(self) -> Optional[str]:
        """
        Loads an XML file from a specified URL
        """
        self.loader = self.loader(self.URL, self.xml_dir, self.xml_file)

        if self.loader.download():
            return self.loader.path_file

        print(self.loader.error)
        return None


class EtrustXmlToDict():
    """
    Class for converting an XML file to a dictionary
    """

    def __init__(self, xml_file_dir: str) -> None:
        self.xml_file_dir = xml_file_dir

    def xml_to_dict(self) -> dict:
        """
        #### Converts an XML file to a dictionary
        """
        tree = ET.parse(self.xml_file_dir)
        root = tree.getroot()

        def parse_element(element):
            if not list(element):
                return element.text

            result = {}
            for key, value in element.attrib.items():
                result[key] = value

            # Processing child elements recursively
            for child in element:
                child_data = parse_element(child)
                child_tag = child.tag
                # If the child element is already in the dictionary, convert it to a list
                if child_tag in result:
                    if not isinstance(result[child_tag], list):
                        result[child_tag] = [result[child_tag]]
                    result[child_tag].append(child_data)
                else:
                    result[child_tag] = child_data

            return result

        return {root.tag: parse_element(root)}


class EtrustParser:
    '''
    Class for parsing a dictionary into objects CA
    '''

    def __init__(self, data: dict) -> None:
        self.data = data

    def parse(self) -> dict[str, str, List[Ca]]:
        """
        #### Parses the data to extract the Ca list and gets the version
        """
        result = {
            "version": None,
            "date_issue": None,
            "ca": [],
        }

        data = self.data

        result["version"] = data['АккредитованныеУдостоверяющиеЦентры'][
            'Версия']
        result["date_issue"] = self._str_to_datetime(
            data['АккредитованныеУдостоверяющиеЦентры']['Дата'],
            '%Y-%m-%dT%H:%M:%S.%fZ')

        centrs = data["АккредитованныеУдостоверяющиеЦентры"][
            "УдостоверяющийЦентр"]

        for centr in centrs:
            result["ca"].append(self._make_ca(centr))

        return result

    def _str_to_datetime(self,
                         string: str,
                         template: str = '%Y-%m-%dT%H:%M:%SZ'):
        """
        #### Converts a string representation of a date and time to a datetime object
        """
        return datetime.strptime(string, template)

    def _check_key(self, dictionary, key):
        """
        #### Check if a given key exists in a dictionary
        """
        if key in dictionary:
            return dictionary[key]
        return None

    def _dict_to_list(self, dictionary):
        """
        #### Convert a dictionary to a list
        """
        if not isinstance(dictionary, list):
            return [dictionary]
        return dictionary

    def _make_ca(self, data) -> Ca:
        """
        #### Creates Ca object
        """
        params = {
            "name":
            data['Название'],
            "email":
            data['ЭлектроннаяПочта'],
            "short_name":
            data['КраткоеНазвание'],
            "about":
            data['АдресСИнформациейПоУЦ'],
            "about_cert":
            self._check_key(data, 'АдресСИнформациейПоРеестрамСертификатов'),
            "address":
            data['Адрес'],
            "inn":
            data['ИНН'],
            "ogrn":
            data['ОГРН'],
            "number":
            data['РеестровыйНомер'],
            "status":
            data['СтатусАккредитации']['Статус'],
            "status_history":
            data['ИсторияСтатусовАккредитации'],
        }

        params["keys"] = self._parse_keys(
            data['ПрограммноАппаратныеКомплексы'])

        ca = Ca(**params)

        return ca

    def _parse_keys(self, data: Union[dict, list]) -> List[Key]:
        """
        #### Parses the data to extract the Key list
        """
        if data is None:
            return None

        softs = self._dict_to_list(data['ПрограммноАппаратныйКомплекс'])
        keys = []

        for soft in softs:
            key_soft = {
                "nickname": soft['Псевдоним'],
                "soft": soft['СредстваУЦ'],
                "soft_class": soft['КлассСредствЭП'],
                "address": str(soft['Адрес']),
            }

            keys = keys + self._make_keys(soft['КлючиУполномоченныхЛиц'],
                                          key_soft)

        return keys

    def _make_keys(
        self,
        data: Union[dict, list],
        soft: dict,
    ) -> list[Key]:
        """
        #### Creates list of Key objects
        """

        if data is None:
            return None

        keys = self._dict_to_list(data['Ключ'])

        result = []
        for key in keys:
            result.append(self._make_key(key, soft))

        return result

    def _make_key(self, data: dict, soft: dict) -> Key:
        """
        #### Creates Key object
        """
        key_id = data['ИдентификаторКлюча']

        params = {
            "key_id": key_id,
            "nickname": soft['nickname'],
            "address": soft['address'],
            "soft": soft['soft'],
            "soft_class": soft['soft_class'],
        }

        params["crls"] = self._parse_crls(data['АдресаСписковОтзыва']['Адрес'],
                                          key_id)
        params["certs"] = self._parse_certs(
            data['Сертификаты']['ДанныеСертификата'], key_id)

        key = Key(**params)

        return key

    def _parse_crls(self, data: Union[dict, list], key_id: str) -> list[Crl]:
        """
        #### Parses data to extract the Crl list
        """

        crls = self._dict_to_list(data)

        result = []
        for crl in crls:
            result.append(self._make_crl(crl, key_id))

        return result

    def _make_crl(self, data: dict, key_id: str) -> Crl:
        """
        #### Creates Crl object
        """
        name_file = unquote(data.rsplit('/', 1)[-1])

        params = {
            "name_file": name_file,
            "url": unquote(data),
            "key_id": key_id,
        }

        crl = Crl(**params)

        return crl

    def _parse_certs(self, data: Union[dict, list], key_id: str) -> list[Cert]:
        """
        #### Parses data to extract the Cert list
        """

        certs = self._dict_to_list(data)
        result = []

        for cert in certs:
            result.append(self._make_cert(cert, key_id))

        return result

    def _make_cert(self, data: dict, key_id: str) -> Cert:
        """
        #### Creates Cert object
        """

        params = {
            "serial": data['СерийныйНомер'],
            "thumbprint": data['Отпечаток'],
            "issued_by": str(data['КемВыдан']),
            "issued_to": str(data['КомуВыдан']),
            "valid_from": self._str_to_datetime(data['ПериодДействияС']),
            "valid_to": self._str_to_datetime(data['ПериодДействияДо']),
            "data_base64": data['Данные'],
            "key_id": key_id,
        }

        cert = Cert(**params)

        return cert
