from object.etrust import Etrust
from builder import EtrustBuilder

etrust = Etrust(**EtrustBuilder().build())

print(etrust.version)
print(etrust.date_issue)
print(etrust.save_file_certs())
print(etrust.save_file_crls())
