import os
from typing import Optional
from parse import EtrustParser
from parse import EtrustLoaderXml
from parse import EtrustXmlToDict


class EtrustBuilder:
    '''
    Class for building data from Etrust
    
    Returns a dictionary
        {
            "version": None,
            "date_issue": None,
            "ca": [],
        }
    '''
    xml_file = "etrust.xml"
    xml_dir = os.path.join(os.getcwd(), "files", "xml")

    def __init__(self, loadfile: bool = True):
        self.loader_xml = EtrustLoaderXml
        self.xml_to_dict = EtrustXmlToDict
        self.parser_xml = EtrustParser
        self.loadfile = loadfile

    def build(self) -> Optional[dict]:
        """
        Builds and returns a dictionary representation of an XML file
        """
        xml_file_dir = os.path.join(self.xml_dir, self.xml_file)

        if self.loadfile:
            if self.loader_xml(self.xml_file, self.xml_dir).get_file():
                data = self.xml_to_dict(xml_file_dir).xml_to_dict()
                return self.parser_xml(data).parse()
        else:
            if os.path.exists(xml_file_dir):
                data = self.xml_to_dict(xml_file_dir).xml_to_dict()
                return self.parser_xml(data).parse()

        return {
            "version": None,
            "date_issue": None,
            "ca": [],
        }
