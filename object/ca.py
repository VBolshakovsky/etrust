from typing import Optional, List
from object.key import Key


class Ca:
    '''
    Certification Authority
    '''

    def __init__(
        self,
        name: Optional[str] = None,
        email: Optional[str] = None,
        short_name: Optional[str] = None,
        about: Optional[str] = None,
        about_cert: Optional[str] = None,
        address: Optional[dict] = None,
        inn: Optional[str] = None,
        ogrn: Optional[str] = None,
        number: Optional[str] = None,
        status: Optional[str] = None,
        status_history: Optional[dict] = None,
        keys: Optional[List[Key]] = None,
    ):

        self.name = name  #Название
        self.email = email  #ЭлектроннаяПочта
        self.short_name = short_name  #КраткоеНазвание
        self.about = about  #АдресСИнформациейПоУЦ
        self.about_cert = about_cert  #АдресСИнформациейПоРеестрамСертификатов
        self.address = address  #Адрес
        self.keys = keys  #ПрограммноАппаратныеКомплексы.КлючиУполномоченныхЛиц
        self.inn = inn  #ИНН
        self.ogrn = ogrn  #ОГРН
        self.number = number  #РеестровыйНомер
        self.status = status  #СтатусАккредитации
        self.status_history = status_history  #ИсторияСтатусовАккредитации
        self.keys = keys

    def __repr__(self):
        return self.short_name

    def __str__(self):
        return self.short_name
