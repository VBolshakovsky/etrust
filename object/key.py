from typing import Optional, List
from object.cert import Cert
from object.crl import Crl


class Key(object):
    '''
    Key certification authority
    '''

    def __init__(
        self,
        key_id: Optional[str] = None,
        crls: Optional[List[Crl]] = None,
        certs: Optional[List[Cert]] = None,
        nickname: Optional[str] = None,
        soft_class: Optional[str] = None,
        address: Optional[dict] = None,
        soft: Optional[str] = None,
    ):

        # КлючиУполномоченныхЛиц
        self.key_id = key_id  # ИдентификаторКлюча
        self.crls = crls  # АдресаСписковОтзыва
        self.certs = certs  # Сертификаты

        # ПрограммноАппаратныйКомплекс
        self.nickname = nickname  #Псевдоним
        self.soft_class = soft_class  #КлассСредствЭП
        self.address = address  #Адрес
        self.soft = soft  #СредстваУЦ

    def __repr__(self):
        return self.key_id

    def __str__(self):
        return self.key_id
