import os
from typing import Optional
from datetime import datetime


class Cert:
    '''
    Root certificate
    '''

    def __init__(
        self,
        serial: Optional[str] = None,
        thumbprint: Optional[str] = None,
        issued_by: Optional[dict] = None,
        issued_to: Optional[dict] = None,
        valid_from: Optional[datetime] = None,
        valid_to: Optional[datetime] = None,
        data_base64: Optional[str] = None,
        key_id: Optional[str] = None,
    ):

        self.serial = serial  # СерийныйНомер
        self.thumbprint = thumbprint  # Отпечаток
        self.issued_by = issued_by  # КемВыдан
        self.issued_to = issued_to  # КомуВыдан
        self.valid_from = valid_from  # ПериодДействияС
        self.valid_to = valid_to  # ПериодДействияДо
        self.data_base64 = data_base64  # Данные
        self.key_id = key_id  # ИдентификаторКлюча

    def __repr__(self):
        return self.serial

    def __str__(self):
        return self.serial

    def save_file_cert(self, path: str) -> str:
        """
        Save the certificate file to the path
        
        Return the name of the file
        """
        extension = ".crt"
        file_path = os.path.join(path, self.serial) + extension

        with open(file_path, "w", encoding="utf-8") as file:
            file.write(self.data_base64)
            file.close()

        return self.serial + extension
