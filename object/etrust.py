import os
import zipfile
from typing import Optional, List
from datetime import datetime
from object.ca import Ca

class Etrust:
    '''
    #### Accredited Certification Authority
    '''

    def __init__(self,
                 version: Optional[str] = None,
                 date_issue: Optional[datetime] = None,
                 ca: Optional[List[Ca]] = None):
        self.version = version
        self.date_issue = date_issue
        self.ca = ca

    def save_file_certs(self,
                        path: Optional[str] = None,
                        create_folder_сa: bool = True,
                        actual: bool = True,
                        create_zip: bool = True) -> None:
        """
        #### Save the certificates of the Certification Authorities
        """

        if not path:
            path = os.path.join(os.getcwd(), "files", "cert")

        if not os.path.exists(path):
            print(f"Directory {path} missing")
            return

        if not self.ca:
            print("List Certification Authority is empty")
            return

        for ca in self.ca:
            if ca.keys:
                for key in ca.keys:
                    if key.certs:
                        for cert in key.certs:
                            if actual and ca.status == "Действует" and datetime.now(
                            ) <= cert.valid_to:
                                if create_folder_сa:
                                    save_path = os.path.join(
                                        path,
                                        ca.short_name.replace(" ",
                                                              "_").replace(
                                                                  "\"", ""))
                                    os.makedirs(save_path, exist_ok=True)
                                    cert.save_file_cert(save_path)
                                else:
                                    cert.save_file_cert(path)

        with open(os.path.join(path,"version.md"), "w", encoding="utf-8") as file_ver:
            file_ver.write(f"Version: {self.version} \nDate: {self.date_issue}")
            file_ver.close()

        if create_zip:
            self._create_zip(path, path, 'ca.zip')

    def save_file_crls(self,
                       path: Optional[str] = None,
                       create_folder_сa: bool = True,
                       actual: bool = True,
                       create_zip: bool = True) -> None:

        if not path:
            path = os.path.join(os.getcwd(), "files", "crl")

        if not os.path.exists(path):
            print(f"Directory {path} missing")
            return

        if not self.ca:
            print("List Certification Authority is empty")
            return

        print("Start download CRLs...")
        for ca in self.ca:
            if ca.keys:
                for key in ca.keys:
                    if key.certs:
                        for crl in key.crls:
                            if actual and ca.status == "Действует":
                                if create_folder_сa:
                                    save_path = os.path.join(
                                        path,
                                        ca.short_name.replace(" ",
                                                              "_").replace(
                                                                  "\"", ""))
                                    os.makedirs(save_path, exist_ok=True)
                                    crl.save_file_crl(save_path)
                                else:
                                    crl.save_file_crl(path)
        print("Finish download CRLs")


        with open(os.path.join(path,"version.md"), "w", encoding="utf-8") as file_ver:
            file_ver.write(f"Version: {self.version} \nDate: {self.date_issue}")
            file_ver.close()

        if create_zip:
            self._create_zip(path, path, 'crl.zip')

    def _create_zip(self, in_path: str, out_path: str, zip_name: str):

        out_zip_path = os.path.join(out_path, zip_name)

        with zipfile.ZipFile(out_zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
            for root, _, files in os.walk(in_path):
                for file in files:
                    if file != zip_name:
                        file_path = os.path.join(root, file)
                        zipf.write(file_path,
                                   os.path.relpath(file_path, in_path))
    
