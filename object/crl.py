from datetime import datetime
from typing import Optional
from url.url import URL
from url.fileloader import Fileloader


class Crl:
    '''
    Crl certificate
    '''

    def __init__(
        self,
        url: Optional[str] = None,
        name_file: Optional[str] = None,
        number: Optional[str] = None,
        oid: Optional[str] = None,
        oid_name: Optional[str] = None,
        last_update: Optional[datetime] = None,
        next_update: Optional[datetime] = None,
        data_base64: Optional[str] = None,
        key_id: Optional[str] = None,
    ):

        self.url = url
        self.name_file = name_file
        self.number = number
        self.oid = oid
        self.oid_name = oid_name
        self.last_update = last_update
        self.next_update = next_update
        self.data_base64 = data_base64
        self.key_id = key_id

    def __repr__(self):
        return self.url

    def __str__(self):
        return self.url

    def save_file_crl(self, path: str) -> str:
        """
        Save the crl-certificate file to the path

        Return the name of the file
        """
        try:
            url = URL(self.url)

            if url.check_url():
                fileload = Fileloader(self.url, path, self.name_file)
                if fileload.download():
                    return self.name_file
                else:
                    print(f"Не скачан:\n{self.url}\nОшибка:{fileload.error}\n")
            else:
                print(f"Не доступен:\n{self.url}\nОшибка:{url.error}\n")
        except Exception as e:
            print(f"Не скачан:\n{self.url}\nОшибка:{e}\n")
        return ""
