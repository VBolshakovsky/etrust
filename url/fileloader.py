from typing import Optional
import os
import requests
from url.url import URL


class FileLoaderError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """


class FileLoader(URL):
    """
        ### Класс для скачивания файлов с url-aдресов

        Аргументы:
            - url: URL-адрес загружаемого файла
            - path: Путь, по которому будет сохранен файл
            - name: Имя сохроняемого файла
    """

    def __init__(self, url: str, path: str, file_name: Optional[str] = None):

        super(FileLoader, self).__init__(url)

        self.path = path
        self.file_name = file_name
        self.path_file = None
        self.size_file = None
        self.error = None

    def _check_path(self, *args: str) -> bool:
        """
        ### Проверка, существуют ли данные пути

        Аргументы:
        - *args: пути, которые необходимо проверить, заполняет self.error если False
        """

        for arg in args:
            if arg is not None and not os.path.exists(arg):
                self.error = f"Директория или файл отсутсвует: {arg}"
                return False
        return True

    def download(self) -> bool:
        """
        Загружает файл по указанному URL-адресу и сохраняет его по локальному пути

        Возврат:
            True, если файл был успешно загружен и сохранен
            - заполняет self.error если False
            - заполняет self.path_file полным путем к файлу
            - заполняет self.size_file размером скаченного файла
        """

        if not self._check_path(self.path):
            return False

        try:
            if self.file_name is None:
                self.file_name = self.get_name()

                if self.file_name is None:
                    return False

            self.path_file = os.path.join(self.path, self.file_name)

            response = requests.get(self.url,
                                    stream=True,
                                    timeout=None,
                                    headers=self.headers)
            response.raise_for_status()

            with open(self.path_file, 'wb') as file:
                for chunk in response.iter_content(chunk_size=8192):
                    if chunk:
                        file.write(chunk)

            self.size_file = os.path.getsize(self.path_file)
            return True

        except FileloadError as error:
            self.error = f"Произошла ошибка при загрузке файла с {self.url}: {error}"

        return False
