#https://docs.python.org/3/library/urllib.parse.html

import requests
from urllib.parse import urlparse
from urllib.parse import unquote
from typing import Optional


class URLError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """


class URL(object):
    """
    ### Класс для работы с URL-адресами
    
    Аргументы:
        - url: URL-адрес
            
    Возврат:    
        - self.error: сообщение об ошибке, если есть, иначе None
    """

    def __init__(self, url: str, headers: Optional[dict] = None):

        self.url = url
        self.error = None
        self.set_default_headers()

    def set_default_headers(self):
        if self.headers is None:
            headers = requests.utils.default_headers()
            headers.update({
                'User-Agent': 'My User Agent 1.0',
            })
            self.headers = headers

    def check_url(self, timeout: int = None) -> bool:
        """
        ### Проверяет, доступен ли URL-адрес

        Аргументы:
        - by_head: отправляет запрос к head, а не get(так быстрее, но не надежней)
        - timeout: количество секунд ожидания, 
            пока клиент установит соединение и/или отправит ответ
            
        Возврат:
            True, если URL-адрес доступен и возвращает код состояния 200
            Заполняет self.error если ошибка
        """

        try:
            response = requests.get(self.url,
                                    timeout=timeout,
                                    headers=self.headers)

            if response.status_code != requests.codes.ok:
                self.error = f"URL {self.url} вернул статусный код {response.status_code}."
                return False
            return True

        except requests.exceptions.RequestException as error:
            self.error = f"Произошла ошибка при проверке доступности URL {self.url}: {error}"

        return False

    def get_name(self, timeout: int = None) -> Optional[str]:
        """
        ### Получает имя скачеваемого файла
        
        Аргументы:
        - timeout: количество секунд ожидания, 
            пока клиент установит соединение и/или отправит ответ
            
        Возврат:
            Имя файла, извлеченное из URL-адреса
            Заполняет self.error если ошибка
        """
        try:
            response = requests.head(self.url, timeout=timeout)
            filename = None
            if 'content-disposition' in response.headers:
                disposition = response.headers['content-disposition']
                parts = disposition.split(';')
                for part in parts:
                    if 'filename' in part:
                        filename = unquote(part.split('=')[1].strip())
            if not filename:
                parsed = urlparse(self.url)
                filename = parsed.path.split("/")[-1]

            return filename

        except URLError as error:
            self.error = f"Ошибка получения имени из URL {self.url}: {error}"

        return None
